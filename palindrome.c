#include <stdio.h>
#include <string.h>

void Palindrome(char str[])
{
    int l = 0;
    int h = strlen(str) - 1;

    while (h > l)
    {
        if (str[l++] != str[h--])
        {
            printf("%s is Not Palindrome\n", str);
            return;
        }
    }
    printf("%s is palindrome\n", str);
}


int main()
{
    Palindrome("malayalam");
    Palindrome("abcdefg");
    return 0;
}
