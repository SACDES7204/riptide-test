import unittest
import palindrome

class TestPalindrome(unittest.TestCase):

    def test_palindrome(self):
        res=palindrome.palindrome("malayalam")
        self.assertEqual(res,True)
    def test_palindrome1(self):
        res=palindrome.palindrome("abcdefg")
        self.assertEqual(res,False)


if __name__ == '__main__':
    unittest.main()
