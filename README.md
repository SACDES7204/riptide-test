# riptide-test
Clone the repo using: git clone@https://gitlab.com/SACDES7204/riptide-test.git

To Run the program, In C:

*  step1: gcc palindrome.c
*  step2: ./a.out

In Python:
* In file palindrome.py i have written a method/function which checks if a string is palindrome or not.
* In file test_palindrome.py i have imported unittest module of python and the palindrome program as well, It has two test cases in it.
* To Execute the Test, Run the below command: python test_palindrome.py
* It returns the test result as ok if passed else results in failures along with the time it took to run the test.


*  A snapshot of the Execution results is also present in this repo.
